<?php

declare(strict_types=1);

namespace App\UI\Quote\Controller;

use App\Domain\Quote\Command\CreateQuote;
use App\Domain\Quote\Entity\Author;
use App\Domain\Quote\Entity\Work;
use App\Domain\Quote\Model\QuoteId;
use App\Domain\Quote\Repository\QuoteRepository;
use App\Infrastructure\Bus\CommandBus\CommandBus;
use App\Infrastructure\Form\RequestToForm;
use App\UI\Quote\Form\QuoteType;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateQuoteController extends AbstractController
{
    public function __construct(
        private RequestToForm $requestToForm,
        private QuoteRepository $quoteRepository,
        private ViewHandlerInterface $viewHandler,
        private CommandBus $commandBus
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $form = $this->requestToForm->createForm(
            QuoteType::class,
            $request
        );

        if (!$form->isValid()) {
            $formErrorsView = View::create($form);
            $formErrorsView->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $this->viewHandler->handle($formErrorsView);
        }

        $newId = QuoteId::generate();

        /** @var Author $author */
        $author = $form->get(QuoteType::AUTHOR_FIELD)->getData();

        /** @var Work $work */
        $work = $form->get(QuoteType::WORK_FIELD)->getData();

        $createQuote = new CreateQuote(
            $newId,
            $author->id()->asString(),
            $form->get(QuoteType::BODY_FIELD)->getData(),
            $form->get(QuoteType::LOCATION_FIELD)->getData(),
            $work->id()->asString()
        );

        $this->commandBus->handle($createQuote);

        $quote = $this->quoteRepository->find($newId->asString());
        $view = View::create($quote);
        $view->getContext()->setGroups(['quote_listing']);

        return $this->viewHandler->handle($view);
    }
}
