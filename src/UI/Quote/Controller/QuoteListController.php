<?php

declare(strict_types=1);

namespace App\UI\Quote\Controller;

use App\Domain\Quote\Repository\QuoteRepository;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class QuoteListController extends AbstractController
{
    public function __construct(
        private ViewHandlerInterface $viewHandler,
        private QuoteRepository $quoteRepository
    ) {
    }

    public function __invoke(): Response
    {
        $quotes = $this->quoteRepository->findAll();

        $view = View::create($quotes);
        $view->getContext()->setGroups(['quote_listing']);

        return $this->viewHandler->handle($view);
    }
}
