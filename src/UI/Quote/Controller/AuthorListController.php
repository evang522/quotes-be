<?php

declare(strict_types=1);

namespace App\UI\Quote\Controller;

use App\Domain\Quote\Repository\AuthorRepository;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AuthorListController extends AbstractController
{
    public function __construct(
        private ViewHandlerInterface $viewHandler,
        private AuthorRepository $authorRepository
    ) {
    }

    public function __invoke(): Response
    {
        $authors = $this->authorRepository->findAll();

        $view = View::create($authors);
        $view->getContext()->setGroups(['minimal_author_listing']);

        return $this->viewHandler->handle($view);
    }
}
