<?php

declare(strict_types=1);

namespace App\UI\Quote\Form;

use App\Domain\Quote\Entity\Author;
use App\Domain\Quote\Entity\Work;
use App\Domain\Tag\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class QuoteType extends AbstractType
{
    public const WORK_FIELD = 'work';
    public const AUTHOR_FIELD = 'author';
    public const LOCATION_FIELD = 'location';
    public const BODY_FIELD = 'body';
    public const TAGS_FIELD = 'tags';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(self::WORK_FIELD, EntityType::class, [
                'class' => Work::class,
                'constraints' => [new NotNull()],
                'error_bubbling' => false,
            ])
            ->add(self::AUTHOR_FIELD, EntityType::class, [
                'class' => Author::class,
                'constraints' => [new NotNull()],
                'error_bubbling' => false,
            ])
            ->add(self::LOCATION_FIELD, TextType::class)
            ->add(self::BODY_FIELD, TextType::class, [
                'constraints' => [new NotNull()],
                'error_bubbling' => false,
            ])
            ->add(self::TAGS_FIELD, CollectionType::class, [
                'entry_type' => EntityType::class,
                'entry_options' => [
                    'class' => Tag::class
                ],
                'error_bubbling' => false,
            ]);
    }
}
