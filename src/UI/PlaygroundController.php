<?php

declare(strict_types=1);

namespace App\UI;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PlaygroundController extends AbstractController
{
    public function __construct()
    {
    }

    public function playAction(Request $request): Response
    {
        return new Response('Playground Controller');
    }
}
