<?php

declare(strict_types=1);

namespace App\Domain\Tag\Entity;

use App\Domain\Tag\Model\TagId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tag")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id",nullable=false)
     */
    private string $id;

    /**
     * @ORM\Column(name="name", nullable=false)
     */
    private string $name;

    public function __construct(
        string $name
    ) {
        $this->name = $name;
        $this->id = TagId::generate()->asString();
    }

    public function id(): TagId
    {
        return TagId::fromString($this->id);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
