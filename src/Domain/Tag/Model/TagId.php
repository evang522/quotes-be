<?php

declare(strict_types=1);

namespace App\Domain\Tag\Model;

use App\Infrastructure\Identity\IdentityTrait;

class TagId
{
    use IdentityTrait;
}
