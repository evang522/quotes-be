<?php

declare(strict_types=1);

namespace App\Domain\Quote\Exception;

use App\Infrastructure\Exception\NotFoundException;

class WorkNotFound extends NotFoundException
{

}
