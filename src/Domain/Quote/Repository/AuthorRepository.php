<?php

declare(strict_types=1);

namespace App\Domain\Quote\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\Persistence\ObjectRepository;

interface AuthorRepository extends ServiceEntityRepositoryInterface, ObjectRepository
{

}
