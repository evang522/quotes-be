<?php

declare(strict_types=1);

namespace App\Domain\Quote\Repository;

use App\Domain\Quote\Entity\Work;
use App\Domain\Quote\Model\WorkId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\Persistence\ObjectRepository;

interface WorkRepository extends ServiceEntityRepositoryInterface, ObjectRepository
{
    public function get(WorkId $workId): Work;

    public function store(Work $work): void;
}
