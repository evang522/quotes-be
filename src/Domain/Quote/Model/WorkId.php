<?php

declare(strict_types=1);

namespace App\Domain\Quote\Model;

use App\Infrastructure\Identity\IdentityTrait;

class WorkId
{
    use IdentityTrait;
}
