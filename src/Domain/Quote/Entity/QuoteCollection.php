<?php

declare(strict_types=1);

namespace App\Domain\Quote\Entity;

use App\Domain\Quote\Model\QuoteCollectionId;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="quote_collection")
 */
class QuoteCollection
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id")
     */
    private string $id;

    /**
     * @var Collection<Quote>
     * @ORM\ManyToMany(targetEntity="App\Domain\Quote\Entity\Quote", cascade="persist")
     */
    private Collection $quotes;

    /**
     * @ORM\Column(name="name", nullable=false, unique=true)
     */
    private string $name;

    public function __construct(string $name)
    {
        $this->id = QuoteCollectionId::generate()->asString();
        $this->name = $name;
        $this->quotes = new ArrayCollection();
    }

    public function name(): string
    {
        return $this->name;
    }

    public function id(): string
    {
        return $this->id;
    }

    /** @return Collection<Quote> */
    public function quotes(): Collection
    {
        return $this->quotes;
    }
}
