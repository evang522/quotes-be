<?php

declare(strict_types=1);

namespace App\Domain\Quote\Entity;

use App\Domain\Quote\Model\AuthorId;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Domain\Quote\Repository\AuthorRepository")
 * @ORM\Table(name="author")
 */
class Author
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="uuid")
     */
    private string $id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private string $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Quote\Entity\Work", cascade="persist", mappedBy="authors")
     * @var Collection<Work> $works
     */
    private Collection $works;

    public function __construct(
        string $name,
    ) {
        $this->id = AuthorId::generate()->asString();
        $this->name = $name;
        $this->works = new ArrayCollection();
    }

    public function id(): AuthorId
    {
        return AuthorId::fromString($this->id);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /** @return Collection<Work> */
    public function works(): Collection
    {
        return $this->works;
    }

    public function setWorks(Collection $works): void
    {
        $this->works = $works;
    }

    public function addWork(): void
    {
    }
}
