<?php

declare(strict_types=1);

namespace App\Domain\Quote\Entity;

use App\Domain\Quote\Model\WorkId;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Domain\Quote\Repository\WorkRepository")
 * @ORM\Table(name="work")
 */
class Work
{
    /**
     * @ORM\Column(type="uuid", name="id")
     * @ORM\Id()
     */
    private string $id;

    /**
     * @ORM\Column(type="string", name="name")
     */
    private string $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Quote\Entity\Author", cascade="persist", inversedBy="works")
     * @var Collection<Author> $authors
     */
    private Collection $authors;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Quote\Entity\Quote", cascade="persist", mappedBy="work")
     * @var Collection<Quote> $quotes
     */
    private Collection $quotes;


    public function __construct(
        string $name
    ) {
        $this->id = WorkId::generate()->asString();
        $this->name = $name;
        $this->authors = new ArrayCollection();
        $this->quotes = new ArrayCollection();
    }


    public function id(): WorkId
    {
        return WorkId::fromString($this->id);
    }

    public function name(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function addAuthor(Author $author): void
    {
        $this->authors->add($author);
    }

    /** @return Collection<Quote> */
    public function quotes(): Collection
    {
        return $this->quotes;
    }

    /** @return Collection<Author> */
    public function authors(): Collection
    {
        return $this->authors;
    }
}
