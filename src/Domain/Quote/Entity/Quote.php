<?php

declare(strict_types=1);

namespace App\Domain\Quote\Entity;

use App\Domain\Quote\Model\QuoteId;
use App\Domain\Tag\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Domain\Quote\Repository\QuoteRepository")
 * @ORM\Table(name="quote")
 */
class Quote
{
    /**
     * @ORM\Column(type="uuid", name="id")
     * @ORM\Id()
     */
    private string $id;

    /**
     * @ORM\Column(type="text", name="body", nullable=true)
     */
    private string|null $body;

    /**
     * @var Work $work
     * @ORM\ManyToOne(targetEntity="App\Domain\Quote\Entity\Work",cascade="persist", inversedBy="quotes")
     */
    private Work $work;

    /**
     * @ORM\Column(name="location", nullable=true)
     */
    private ?string $location;

    /**
     * @var Collection<Tag>
     * @ORM\ManyToMany(cascade="persist", targetEntity="App\Domain\Tag\Entity\Tag")
     */
    private Collection $tags;

    public function __construct(Work $work)
    {
        $this->id = QuoteId::generate()->asString();
        $this->work = $work;
        $this->body = null;
        $this->location = null;
        $this->tags = new ArrayCollection();
    }

    public function location(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): void
    {
        $this->location = $location;
    }

    public function id(): QuoteId
    {
        return QuoteId::fromString($this->id);
    }

    public function setId(QuoteId $id): void
    {
        $this->id = $id->asString();
    }

    public function work(): Work
    {
        return $this->work;
    }

    public function body(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): void
    {
        $this->body = $body;
    }

    /** @return Collection<Tag> */
    public function tags(): Collection
    {
        return $this->tags;
    }

    /**
     * @param Collection<Tag> $tags
     */
    public function setTags(Collection $tags): void
    {
        $this->tags = $tags;
    }

    public function addTag(Tag $tag): void
    {
        $this->tags->add($tag);
    }
}
