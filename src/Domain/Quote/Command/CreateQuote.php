<?php

declare(strict_types=1);

namespace App\Domain\Quote\Command;

use App\Domain\Quote\Model\QuoteId;

class CreateQuote
{
    private QuoteId $newIdentity;
    private string $authorId;
    private string $body;
    private ?string $location;
    private string $workId;

    public function __construct(
        QuoteId $newIdentity,
        string $authorId,
        string $body,
        ?string $location,
        string $workId
    ) {
        $this->newIdentity = $newIdentity;
        $this->authorId = $authorId;
        $this->body = $body;
        $this->location = $location;
        $this->workId = $workId;
    }

    public function authorId(): string
    {
        return $this->authorId;
    }

    public function body(): string
    {
        return $this->body;
    }

    public function location(): ?string
    {
        return $this->location;
    }

    public function workId(): string
    {
        return $this->workId;
    }

    public function newIdentity(): QuoteId
    {
        return $this->newIdentity;
    }
}
