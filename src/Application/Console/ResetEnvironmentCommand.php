<?php

declare(strict_types=1);

namespace App\Application\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ResetEnvironmentCommand extends Command
{
    private SymfonyStyle $io;

    private string $environment;

    public function __construct(
        string $environment
    ) {
        $this->environment = $environment;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('quotes:dev:reset:env')
            ->setDescription('Reset completely the environment.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        if ($this->environment === 'prod') {
            $this->io->error('You can not reset the production environment');

            return 0;
        }

        $confirm = $this->io->confirm(
            \sprintf('You are about to reset the "%s" environment. Are you sure ?', $this->environment)
        );
        if ($confirm !== true) {
            $this->io->note('Aborted');

            return 0;
        }

        $this->resetDatabase();
        $this->loadFixtures();

        $this->io->success(\sprintf('Finished resetting completely the "%s" environment.', $this->environment));

        return 0;
    }

    private function resetDatabase(): void
    {
        $this->io->section('Resetting the database');

        $this->io->text('Dropping migrations table.');
        $this->runCommand('dbal:run-sql', ['sql' => 'DROP TABLE IF EXISTS doctrine_migration_versions']);

        $this->io->text('Dropping schema.');
        $this->runCommand('doctrine:schema:drop', ['--force' => true]);

        $this->io->text('Running migrations.');
        $this->runCommand('doctrine:migrations:migrate');
    }

    private function runCommand(string $name, array $options = []): bool
    {
        $command = $this->getApplication()->get($name);
        $command->mergeApplicationDefinition();
        $command = clone $command;

        $parameters = \array_merge($options, ['command' => $name, '--env' => $this->environment]);
        $input = new ArrayInput($parameters);
        $input->setInteractive(false);

        $returnCode = $command->run($input, new NullOutput());

        return $returnCode === 0;
    }

    private function loadFixtures(): void
    {
        $this->io->section('Development fixtures');

        $this->io->text('Loading fixtures.');
        $this->runCommand('doctrine:fixtures:load', ['--append' => '']);
    }
}
