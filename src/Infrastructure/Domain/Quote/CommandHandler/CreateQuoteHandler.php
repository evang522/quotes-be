<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Quote\CommandHandler;

use App\Domain\Quote\Command\CreateQuote;
use App\Domain\Quote\Entity\Quote;
use App\Domain\Quote\Model\WorkId;
use App\Infrastructure\Domain\Quote\Repository\QuoteRepository;
use App\Infrastructure\Domain\Quote\Repository\WorkRepository;

class CreateQuoteHandler
{

    public function __construct(
        private QuoteRepository $quoteRepository,
        private WorkRepository $workRepository
    ) {
    }

    public function __invoke(CreateQuote $command): void
    {
        $work = $this->workRepository->get(WorkId::fromString($command->workId()));

        $quote = new Quote($work);
        $quote->setId($command->newIdentity());
        $quote->setLocation($command->location());
        $quote->setBody($command->body());

        $this->quoteRepository->store($quote);
    }
}
