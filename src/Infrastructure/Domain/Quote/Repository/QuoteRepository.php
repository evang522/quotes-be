<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Quote\Repository;

use App\Domain\Quote\Entity\Quote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class QuoteRepository extends ServiceEntityRepository implements \App\Domain\Quote\Repository\QuoteRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Quote::class);
    }

    public function store(Quote $quote): void
    {
        $this->getEntityManager()->persist($quote);
        $this->getEntityManager()->flush();
    }
}
