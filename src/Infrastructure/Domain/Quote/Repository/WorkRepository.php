<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Quote\Repository;

use App\Domain\Quote\Entity\Work;
use App\Domain\Quote\Exception\WorkNotFound;
use App\Domain\Quote\Model\WorkId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WorkRepository extends ServiceEntityRepository implements \App\Domain\Quote\Repository\WorkRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Work::class);
    }

    public function store(Work $work): void
    {
        $this->getEntityManager()->persist($work);
        $this->getEntityManager()->flush();
    }

    public function get(WorkId $workId): Work
    {
        $work = $this->find($workId->asString());

        if ($work === null) {
            throw new WorkNotFound('Work with id "' . $workId->asString() . '" not found');
        }

        return $work;
    }
}
