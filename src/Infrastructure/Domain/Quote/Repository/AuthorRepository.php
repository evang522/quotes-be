<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Quote\Repository;

use App\Domain\Quote\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AuthorRepository extends ServiceEntityRepository implements \App\Domain\Quote\Repository\AuthorRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Author::class);
    }

    public function store(Author $author): void
    {
        $this->getEntityManager()->persist($author);
        $this->getEntityManager()->flush();
    }
}
