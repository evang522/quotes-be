<?php

declare(strict_types=1);

namespace App\Infrastructure\Form;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class RequestToForm
{
    public function __construct(
        private FormFactoryInterface $formFactory
    ) {
    }

    public function createForm(string $formType, Request $request, array $options = []): FormInterface
    {
        $form = $this->formFactory->create(
            $formType,
            null,
            $options
        );

        $form->submit(
            array_replace_recursive(
                $request->files->all(),
                $request->query->all(),
                $request->request->all()
            )
        );

        return $form;
    }
}
