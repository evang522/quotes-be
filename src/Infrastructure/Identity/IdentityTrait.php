<?php

declare(strict_types=1);

namespace App\Infrastructure\Identity;

use JetBrains\PhpStorm\Pure;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;

trait IdentityTrait
{
    public function __construct(
        private Uuid $uuid
    ) {
    }

    public static function fromString(string $uuid): static
    {
        return new static(UuidV4::fromString($uuid));
    }

    public static function generate(): static
    {
        return new static(UuidV4::v4());
    }

    #[Pure] public function equals(IdentityTrait $identity): bool
    {
        return $identity instanceof static && $identity->uuid->equals($this->uuid);
    }

    public function __toString(): string
    {
        return $this->asString();
    }

    #[Pure] public function asString(): string
    {
        return $this->uuid->__toString();
    }
}
