<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210514141817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Quotes, Authors, Works, Tags, and Collections';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE author (id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN author.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE quote (id UUID NOT NULL, work_id UUID DEFAULT NULL, body TEXT DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6B71CBF4BB3453DB ON quote (work_id)');
        $this->addSql('COMMENT ON COLUMN quote.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN quote.work_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE quote_tag (quote_id UUID NOT NULL, tag_id VARCHAR(255) NOT NULL, PRIMARY KEY(quote_id, tag_id))');
        $this->addSql('CREATE INDEX IDX_3658A64FDB805178 ON quote_tag (quote_id)');
        $this->addSql('CREATE INDEX IDX_3658A64FBAD26311 ON quote_tag (tag_id)');
        $this->addSql('COMMENT ON COLUMN quote_tag.quote_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE quote_collection (id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C1A1F895E237E06 ON quote_collection (name)');
        $this->addSql('CREATE TABLE quote_collection_quote (quote_collection_id VARCHAR(255) NOT NULL, quote_id UUID NOT NULL, PRIMARY KEY(quote_collection_id, quote_id))');
        $this->addSql('CREATE INDEX IDX_7C62A174B89D1297 ON quote_collection_quote (quote_collection_id)');
        $this->addSql('CREATE INDEX IDX_7C62A174DB805178 ON quote_collection_quote (quote_id)');
        $this->addSql('COMMENT ON COLUMN quote_collection_quote.quote_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE tag (id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE work (id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN work.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE work_author (work_id UUID NOT NULL, author_id UUID NOT NULL, PRIMARY KEY(work_id, author_id))');
        $this->addSql('CREATE INDEX IDX_16561EEABB3453DB ON work_author (work_id)');
        $this->addSql('CREATE INDEX IDX_16561EEAF675F31B ON work_author (author_id)');
        $this->addSql('COMMENT ON COLUMN work_author.work_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN work_author.author_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE quote ADD CONSTRAINT FK_6B71CBF4BB3453DB FOREIGN KEY (work_id) REFERENCES work (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quote_tag ADD CONSTRAINT FK_3658A64FDB805178 FOREIGN KEY (quote_id) REFERENCES quote (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quote_tag ADD CONSTRAINT FK_3658A64FBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quote_collection_quote ADD CONSTRAINT FK_7C62A174B89D1297 FOREIGN KEY (quote_collection_id) REFERENCES quote_collection (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quote_collection_quote ADD CONSTRAINT FK_7C62A174DB805178 FOREIGN KEY (quote_id) REFERENCES quote (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_author ADD CONSTRAINT FK_16561EEABB3453DB FOREIGN KEY (work_id) REFERENCES work (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE work_author ADD CONSTRAINT FK_16561EEAF675F31B FOREIGN KEY (author_id) REFERENCES author (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }
}
