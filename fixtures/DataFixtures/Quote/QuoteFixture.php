<?php

declare(strict_types=1);


namespace App\DataFixtures\Quote;

use App\DataFixtures\Tag\TagFixture;
use App\Domain\Quote\Entity\Author;
use App\Domain\Quote\Entity\Quote;
use App\Domain\Quote\Entity\Work;
use App\Domain\Tag\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class QuoteFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $allQuotes = array_merge($this->getCsLewisQuotes(), $this->getJohnStuartMillQuotes());

        foreach ($allQuotes as $quote) {

            $newQuote = new Quote($quote['work']);
            $newQuote->setLocation($quote['location']);
            $newQuote->setBody($quote['quote']);
            foreach ($quote['tags'] as $tag) {
                /** @var Tag $referencedTag */
                $referencedTag = $this->getReference('tag_' . $tag);
                $newQuote->addTag($referencedTag);
            }

            $manager->persist($newQuote);
        }

        $manager->flush();
    }

    private function getCsLewisQuotes(): array
    {
        $author = new Author('C.S. Lewis');
        $theGreatDivorce = new Work('The Great Divorce');
        $theGreatDivorce->addAuthor($author);

        return [
            [
                'work' => $theGreatDivorce,
                'quote' => 'Redeemed humanity is still young, it has hardly come to its full strength, but already there is joy enough in the finger of a great saint such as yonder lady to awaken all the dead things in the universe to life.',
                'location' => 'Chapter 12',
                'tags' => ['joy', 'inspiration']
            ],
            [
                'work' => $theGreatDivorce,
                'quote' => 'If I could remember their singing and write down the notes, no man who read that score would ever grow sick or old.',
                'location' => 'Chapter 12',
                'tags' => ['joy', 'inspiration']

            ],
            [
                'work' => $theGreatDivorce,
                'quote' => 'They say of some temporal suffering, “No future bliss can make up for it,” not knowing that Heaven, once attained, will work backwards and turn even that agony into a glory.',
                'location' => 'Chapter 8',
                'tags' => ['joy', 'inspiration', 'theology']
            ]
        ];
    }


    private function getJohnStuartMillQuotes(): array
    {
        $author = new Author('John Stuart Mill');
        $onLiberty = new Work('On Liberty');
        $onLiberty->addAuthor($author);

        return [
            [
                'work' => $onLiberty,
                'quote' => 'Judgment is given to men that they may use it. Because it may be used erroneously, are men to be told that they ought not to use it at all?',
                'location' => 'Page 43',
                'tags' => ['philosophy', 'liberty']
            ],
            [
                'work' => $onLiberty,
                'quote' => 'The whole strength and value, then, of human judgment, depending on the one property, that it can be set right when it is wrong, reliance can be placed on it only when the means of setting it right are kept constantly at hand. In the case of any person whose judgment is really deserving of confidence, how has it become so? Because he has kept his mind open to criticism of his opinions and conduct. Because it has been his practice to listen to all that could be said against him; to profit by as much of it as was just, and expound to himself, and upon occasion to others, the fallacy of what was fallacious. Because he has felt, that the only way in which a human being can make some approach to knowing the whole of a subject, is by hearing what can be said about it by persons of every variety of opinion, and studying all modes in which it can be looked at by every character of mind. No wise man ever acquired his wisdom in any mode but this; nor is it in the nature of human intellect to become wise in any other manner. The steady habit of correcting and completing his own opinion by collating it with those of others, so far from causing doubt and hesitation in carrying it into practice, is the only stable foundation for a just reliance on it: for, being cognisant of all that can, at least obviously, be said against him, and having taken up his position against all gainsayers—knowing that he has sought for objections and difficulties, instead of avoiding them, and has shut out no light which can be thrown upon the subject from any quarter—he has a right to think his judgment better than that of any person, or any multitude, who have not gone through a similar process.',
                'location' => 'Page 200',
                'tags' => ['wisdom', 'philosophy']
            ],
        ];
    }

    /** @return string[] */
    public function getDependencies(): array
    {
        return [
            TagFixture::class
        ];
    }
}
