<?php

declare(strict_types=1);


namespace App\DataFixtures\Tag;


use App\Domain\Tag\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TagFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        foreach ($this->getTags() as $tagName) {
            $tagObject = new Tag($tagName);
            $manager->persist($tagObject);
            $this->setReference('tag_' . strtolower($tagName), $tagObject);
        }

        $manager->flush();
    }

    private function getTags(): array
    {
        return [
            'Inspiration',
            'History',
            'Philosophy',
            'Joy',
            'Theology',
            'Apostolic Authority',
            'Sola Scriptura',
            'Evolutionary Theory',
            'Naturalism',
            'Eucharist',
            'Apostolic Fathers',
            'Joy',
            'Wisdom',
            'Liberty'
        ];
    }
}
